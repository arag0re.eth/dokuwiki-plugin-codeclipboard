# dokuwiki-plugin-codeclipboard

Official plugin page: [codeclipboard](https://www.dokuwiki.org/plugin:codeclipboard)

With this plugin enabled you can copy code block content within the frontend
with a simple button.

![screenshot](img/screenshot.png)

## Design

Based on an Action plugin it will load a javascript code that uses jQuery
to call the copy command on a fake textarea.

## HowTo

Install from the extension page within your Dokuwiki configuration page or
decompress in your plugin folder `/dokuwiki/lib/plugins`

## Development

Use Docker to setup a Dokuwiki instance

`docker-compose.yml`

```
version: '2'
services:
  dokuwiki:
    image: 'docker.io/bitnami/dokuwiki:0-debian-10'
    ports:
      - '8080:80'
    volumes:
      - 'dokuwiki_data:/bitnami'
      - '~/dokuwiki-plugin-codeclipboard:/bitnami/dokuwiki/lib/plugins/codeclipboard/'
volumes:
  dokuwiki_data:
    driver: local
```

